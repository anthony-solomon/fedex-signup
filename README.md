# FedexSignup

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.0.0.

## Development notes

- Created git branches for each feature to isolate the different streams of work
- Pushed in to the feature branch until I was ready to perform a PR in to the master branch

## Implementation notes

1. Create a working signup form ensuring that the basic functionality works

- Setup the form and the form fields
- Validation - Implement the validation including:
  - Ensure that all of the fields are required/mandatory
  - Regex validation for the email and password
  - Password confirmation validation
- Encapsulate the signup form to a component
- Emit the data when the submit button is pressed
- Export and embed the form in the app.component (Did not create routing due to the simplicity of the application)

2. Submit the data to the endpoint

- Create an interface is created for the payload
- Create a service and expose a method that posts to the service endpoint
- Listen for the form submission
- Invoke the service method passing in the form data

3. React to the server-side response

- Display an error message when a problem occured
- Display a success message when the submission was successful

4. Architecture decisions

- Using a service to embed the logic to invoke the endpoint
- Store the URL in an environment variable that would allow us to change it when we deploy to dev, staging and production

5. Installed Packages

- Decided not to use a UI framework to style the form (Material or Bootstrap) because I felt that it was overkill. I chose to use the default styling from the schematic generated application and some additional styles
- Installed eslint to assist with code consistency and to reduce code smells
- Installed prettier to automatically format code to a specific set of rules to enforce consistency

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Linting code

Run `ng lint` to perform linting on project. Installed eslint because Angular 12 no longer comes with a linter built in.
- Kindly note that the ruleset is not complete but I wanted to demonstrate the installation and usage of linting for Angular 12 (since TSLint has been deprecated)
- Installed prettier to enforce the rules from eslint - have found this to be incredibly useful to enforce standards before the PR disputes.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
