import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { PASSWORD_MIN_CHARACTER_COUNT, SignupStatus } from '@shared/constants';
import { UserPayload } from '@shared/interface/user-payload.interface';
import { Observable, Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';
import {
  emailValidator,
  passwordMatchValidator,
  passwordMinCharacterCountValidator,
  passwordMustContainUppperAndLowerCaseValidator,
  passwordMustNotContainNameValidator
} from '../../data/validation';

@Component({
  selector: 'app-signup-form',
  templateUrl: './signup-form.component.html',
  styleUrls: ['./signup-form.component.scss']
})
export class SignupFormComponent implements OnInit, OnDestroy {
  @Input() signupStatus$!: Observable<SignupStatus>;
  @Output() formDataSubmitted$ = new EventEmitter<UserPayload>();

  readonly signupForm: FormGroup;

  readonly passwordErrorConfig = {
    required: 'Please provide a password',
    passwordMinCharacterCount: "Your password must be at least 8 characters long",
    passwordMustContainerUpperAndLowerCase: "Your password must contain upper and lowercase letters",
    passwordMustNotContainName: "Your password must not contain either of your names"
  };

  readonly passwordConfirmErrorConfig = {
    required: 'Please confirm your password',
    confirmPassword: 'Your confirm password needs to match the original password'
  };

  readonly emailErrorConfig = {
    email: 'Please provide a valid email address'
  };

  private readonly unsubscribe$ = new Subject();

  constructor() {
    this.signupForm = new FormGroup({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, emailValidator]),
      password: new FormControl('', [
        Validators.required,
        passwordMinCharacterCountValidator(PASSWORD_MIN_CHARACTER_COUNT),
        passwordMustContainUppperAndLowerCaseValidator
      ]),
      confirmPassword: new FormControl('', [Validators.required, ])
    }, [
      passwordMustNotContainNameValidator('firstName', 'lastName', 'password'),
      passwordMatchValidator('password', 'confirmPassword')
    ]);

  }
  ngOnInit(): void {
    this.signupStatus$
    .pipe(
      takeUntil(this.unsubscribe$),
      filter((status: SignupStatus) => (status === SignupStatus.Success))
    )
    .subscribe(() => {
      this.resetForm();
    });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  submitForm(): void {
    this.formDataSubmitted$.emit({
      firstName: this.signupForm.get('firstName')?.value,
      lastName: this.signupForm.get('lastName')?.value,
      email: this.signupForm.get('email')?.value,
    });
  }

  private resetForm(): void {
    this.signupForm.reset();
    this.signupForm.markAsPristine();
  }
}
