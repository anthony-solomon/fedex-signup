import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { ComponentModule } from '@shared/component/component.module';
import { SignupFormComponent } from './component/signup-form/signup-form.component';

@NgModule({
  declarations: [
    SignupFormComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ComponentModule
  ],
  exports: [
    SignupFormComponent
  ]
})
export class MembershipModule { }
