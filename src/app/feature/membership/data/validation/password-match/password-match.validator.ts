import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export function passwordMatchValidator(passwordFieldName: string, confirmPasswordFieldName: string): ValidatorFn {
  return (formGroup: AbstractControl): ValidationErrors | null => {
    if (!formGroup) {
      return null;
    }
    const password = formGroup.get(passwordFieldName)?.value;
    const confirmPassword = formGroup.get(confirmPasswordFieldName)?.value;

    if (!password || !confirmPassword) {
      return null;
    }

    return (password === confirmPassword)
      ? null
      : { confirmPassword: true };
  }
}
