import { AbstractControl, FormControl, FormGroup } from '@angular/forms';
import { passwordMatchValidator } from './password-match.validator';

const PASSWORD_CONTROL_NAME = 'password';
const CONFIRM_PASSWORD_CONTROL_NAME = 'confirmPassword';

const PASSWORD = "SRDWGSdfda32#$%$#";
const DIFFERENT_PASSWORD = "^%SCVdcsdfewth**{ZX~`";

describe('passwordMatchValidator', () => {
  var formUnderTest: FormGroup;
  var passwordControl: AbstractControl | null;
  var confirmPasswordControl: AbstractControl | null;

  beforeEach(() => {
    formUnderTest = new FormGroup({
      password: new FormControl(''),
      confirmPassword: new FormControl(''),
    }, passwordMatchValidator(PASSWORD_CONTROL_NAME, CONFIRM_PASSWORD_CONTROL_NAME));
    passwordControl = formUnderTest.get(PASSWORD_CONTROL_NAME);
    confirmPasswordControl = formUnderTest.get(CONFIRM_PASSWORD_CONTROL_NAME);
  });

  it('should return null when both password values match', () => {
    passwordControl?.setValue(PASSWORD);
    confirmPasswordControl?.setValue(PASSWORD);
    expect(formUnderTest.valid).toBeTrue();
  });

  it('should fail when the password values do NOT match', () => {
    passwordControl?.setValue(PASSWORD);
    confirmPasswordControl?.setValue(DIFFERENT_PASSWORD);
    expect(formUnderTest.valid).toBeFalse();
    expect(formUnderTest?.hasError('confirmPassword')).toBeTrue();

    
    passwordControl?.setValue(DIFFERENT_PASSWORD);
    confirmPasswordControl?.setValue(PASSWORD);
    expect(formUnderTest.valid).toBeFalse();
    expect(formUnderTest?.hasError('confirmPassword')).toBeTrue();
  });
});

