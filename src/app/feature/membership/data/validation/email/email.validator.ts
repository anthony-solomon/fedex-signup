import { AbstractControl, ValidationErrors } from '@angular/forms';
import { isValidEmail } from '@shared/validation';

export function emailValidator(control: AbstractControl): ValidationErrors | null {
  if (!control.value) {
    return null;
  }
  return isValidEmail(control.value)
    ? null
    : { email: true };
}
