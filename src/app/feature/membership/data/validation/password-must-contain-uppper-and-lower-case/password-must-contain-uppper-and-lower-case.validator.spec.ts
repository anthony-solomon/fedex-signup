import { FormControl } from '@angular/forms';
import { passwordMustContainUppperAndLowerCaseValidator } from './password-must-contain-uppper-and-lower-case.validator';

describe('passwordMustNotContainNameValidator', () => {
  it('should be valid when there are uppercase and lowercase characters in the password', () => {
    const passwordControl = new FormControl('ThisIsAValidPassword');
    const validationResult = passwordMustContainUppperAndLowerCaseValidator(passwordControl);
    expect(validationResult).toBeNull();
  });

  // We only need this one test to check that the validation is working because we have 
  // already testwed the business logic and the variations in the helper method:
  // hasUpperAndLowercaseLetters(value: string): boolean method
  it('should fail when there are not uppercase and lowercase values in the password', () => {
    const passwordControl = new FormControl('');

    passwordControl.setValue('aaaaaaaa');
    expect(passwordMustContainUppperAndLowerCaseValidator(passwordControl))
      .toEqual({ passwordMustContainerUpperAndLowerCase: true });

    passwordControl.setValue('AAAAAAAA');
    expect(passwordMustContainUppperAndLowerCaseValidator(passwordControl))
      .toEqual({ passwordMustContainerUpperAndLowerCase: true });
  });
});

