import { AbstractControl, ValidationErrors } from '@angular/forms';
import { hasUpperAndLowercaseLetters } from '@shared/validation';

export function passwordMustContainUppperAndLowerCaseValidator(passwordControl: AbstractControl): ValidationErrors | null {
  if (!passwordControl || !passwordControl.value) {
    return null;
  }
  return (!hasUpperAndLowercaseLetters(passwordControl.value))
    ? { passwordMustContainerUpperAndLowerCase: true }
    : null;
}
