import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

export function passwordMustNotContainNameValidator(firstNameFieldName: string, lastNameFieldName: string,
    passwordFieldName: string): ValidatorFn {
  return (formGroup: AbstractControl): ValidationErrors | null => {
    const firstName = formGroup?.get(firstNameFieldName)?.value;
    const lastName = formGroup?.get(lastNameFieldName)?.value;
    const password = formGroup?.get(passwordFieldName)?.value;

    if (!password || (!firstName && !lastName)) {
      return null;
    }

    return (firstName && password.includes(firstName)) || (lastName && password.includes(lastName))
      ? { passwordMustNotContainName: true }
      : null;
  }
}
