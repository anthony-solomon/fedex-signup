import { AbstractControl, FormControl, FormGroup } from '@angular/forms';
import { passwordMustNotContainNameValidator } from './password-must-not-contain-name.validator';

const FIRST_NAME_CONTROL_NAME = 'firstName';
const LAST_NAME_CONTROL_NAME = 'lastName';
const PASSWORD_CONTROL_NAME = 'password';

describe('passwordMustNotContainNameValidator', () => {
  var formUnderTest: FormGroup;
  var firstNameControl: AbstractControl | null;
  var lastNameControl: AbstractControl | null;
  var passwordControl: AbstractControl | null;

  beforeEach(() => {
    formUnderTest = new FormGroup({
      firstName: new FormControl('FirstName'),
      lastName: new FormControl('LastName'),
      password: new FormControl('')
    }, passwordMustNotContainNameValidator(FIRST_NAME_CONTROL_NAME, LAST_NAME_CONTROL_NAME, PASSWORD_CONTROL_NAME));

    firstNameControl = formUnderTest.get(FIRST_NAME_CONTROL_NAME);
    lastNameControl = formUnderTest.get(LAST_NAME_CONTROL_NAME);
    passwordControl = formUnderTest.get(PASSWORD_CONTROL_NAME);
  });

  it('should be valid when there is no name in the password', () => {
    passwordControl?.setValue('ThisIsAValidPassword');
    expect(formUnderTest.valid).toBeTrue();
  });

  it('should fail when the password is the same as the first name', () => {
    passwordControl?.setValue(firstNameControl?.value);
    expect(formUnderTest?.valid).toBeFalse();
    expect(formUnderTest?.hasError('passwordMustNotContainName')).toBeTrue();
  });

  it('should fail when the password contains the first name', () => {
    passwordControl?.setValue(`XXXMANDK${firstNameControl?.value}123dcSCAAS`);
    expect(formUnderTest?.valid).toBeFalse();
    expect(formUnderTest?.hasError('passwordMustNotContainName')).toBeTrue();
  });

  it('should fail when the password is the same as the last name', () => {
    passwordControl?.setValue(lastNameControl?.value);
    expect(formUnderTest?.valid).toBeFalse();
    expect(formUnderTest?.hasError('passwordMustNotContainName')).toBeTrue();
  });

  it('should fail when the password contains the last name', () => {
    passwordControl?.setValue(`XXXMANDK${lastNameControl?.value}123dcSCAAS`);
    expect(formUnderTest?.valid).toBeFalse();
    expect(formUnderTest?.hasError('passwordMustNotContainName')).toBeTrue();
  });
});

