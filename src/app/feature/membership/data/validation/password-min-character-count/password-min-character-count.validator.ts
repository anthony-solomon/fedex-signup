import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';
import { hasMinimumCharacterCount } from 'src/app/shared/validation';

export function passwordMinCharacterCountValidator(minCharacterCount: number): ValidatorFn {
  return (passwordControl: AbstractControl): ValidationErrors | null => {
    if (!passwordControl.value) {
      return null;
    }
    return hasMinimumCharacterCount(passwordControl.value, minCharacterCount)
      ? null
      : { passwordMinCharacterCount: true };
  }
}
