import { FormControl } from '@angular/forms';
import { PASSWORD_MIN_CHARACTER_COUNT } from '@shared/constants';
import { passwordMinCharacterCountValidator } from './password-min-character-count.validator';

describe('passwordMinCharacterCountValidator', () => {
  var passwordInputFormControl: FormControl;

  beforeEach(() => {
    passwordInputFormControl = new FormControl(null, passwordMinCharacterCountValidator(PASSWORD_MIN_CHARACTER_COUNT));
  });

  it('should pass when there is no value', () => {
    // Validate for the default 'null' value 
    expect(passwordInputFormControl.valid).toBeTrue();

    passwordInputFormControl.setValue('');
    expect(passwordInputFormControl.valid).toBeTrue();
  });

  it('should pass when the value contains the minimum character count (or more)', () => {
    const checkValue = "X".repeat(PASSWORD_MIN_CHARACTER_COUNT);

    passwordInputFormControl.setValue(checkValue);
    expect(passwordInputFormControl.valid).toBeTrue();

    passwordInputFormControl.setValue(checkValue + 'XXX');
    expect(passwordInputFormControl.valid).toBeTrue();
  });

  it('should fail when the value contains less than the minimum character count', () => {
    const checkValue = "X".repeat(PASSWORD_MIN_CHARACTER_COUNT - 1);

    passwordInputFormControl.setValue(checkValue);
    expect(passwordInputFormControl.valid).toBeFalse();
    expect(passwordInputFormControl.hasError('passwordMinCharacterCount')).toBeTrue();
  });
});
