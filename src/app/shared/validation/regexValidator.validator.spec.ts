import { FormControl, ValidatorFn } from '@angular/forms';
import { regexValidator } from './regexValidator.validator';

describe('regexValidator', () => {
  const regEx = /\d/;
  var testFormControl: FormControl;
  var regExTester: ValidatorFn;
  var mockValidationErrors: { 'error': 'This is an error' };

  beforeEach(() => {
    regExTester = regexValidator(regEx, mockValidationErrors);
    testFormControl = new FormControl('');
  });

  it('should test the value of the form control using any specified regular expression', () => {
    testFormControl.setValue(12345);
    expect(regExTester(testFormControl)).toEqual(null);

    testFormControl.setValue('XXXX');
    expect(regExTester(testFormControl)).toEqual(mockValidationErrors);
  });
});
