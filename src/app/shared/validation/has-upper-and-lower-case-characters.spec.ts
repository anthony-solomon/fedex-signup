import { hasUpperAndLowercaseLetters } from './has-upper-and-lower-case-characters';

describe('hasUpperAndLowercaseLetters', () => {
  it('should return true for passwords that contain uppercase and lowercase characters', () => {
    expect(hasUpperAndLowercaseLetters('Zz')).toBe(true);
  });

  it('should return false for passwords that only contain lowercase characters', () => {
    expect(hasUpperAndLowercaseLetters('z')).toBe(false);
  });

  it('should return false for passwords that only contain uppercase characters', () => {
    expect(hasUpperAndLowercaseLetters('Z')).toBe(false);
  });

  it('should return false for passwords that only consist of numbers', () => {
    expect(hasUpperAndLowercaseLetters('123')).toBe(false);
  });

  it('should return false for passwords that only consist of non alpha-numeric characters', () => {
    expect(hasUpperAndLowercaseLetters('!@#$%^&*()|":')).toBe(false);
  });
});
