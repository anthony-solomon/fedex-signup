// Encapsulated this out to allow reuse across multiple validators
export function hasUpperAndLowercaseLetters(value: string): boolean {
  const upperCaseRegEx = /[A-Z]/;
  const lowerCaseRegEx = /[a-z]/;
  return upperCaseRegEx.test(value) && lowerCaseRegEx.test(value);
}