import { isValidEmail } from './is-valid-email';

const validEmails = [
  'a@a.com',
  'a@a.co.uk',
  'a.a@a.com',
  'a.a@a.co.uk',
  'a@a.online'
];

const invalidEmails = [
  '@a.com',
  'a@a',
  'a.com',
  'aaa',
  '.com'
];

describe('isValidEmail', () => {
  it('should return true for all valid email addresses', () => {
    for (const validEmail of validEmails) {
      expect(isValidEmail(validEmail)).toEqual(true);
    }
  });

  it('should return false for all invalid email addresses', () => {
    for (const invalidEmail of invalidEmails) {
      expect(isValidEmail(invalidEmail)).toEqual(false);
    }
  });
});

