import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

/**
   * A generic validator that wraps the regular expression testing in to a validator function 
   * that can be re-used in other validators
   * @param {RegExp} regex - The regular expression to be used to test the value
   * @param {number} error - The error that will be raised/returned when the value fails against the regular expression
   * @returns {ValidatorFn} - Returns the validator function that can be used to validator form
*/
export function regexValidator(regex: RegExp, error: ValidationErrors): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    if (!control.value) {
      return null;
    }
    const valid = regex.test(control.value);
    return valid ? null : error;
  };
}