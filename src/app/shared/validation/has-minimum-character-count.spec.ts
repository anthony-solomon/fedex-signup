import { hasMinimumCharacterCount } from './has-minimum-character-count';

describe('hasMinimumCharacterCount', () => {
  it('should return true when there is no value', () => {
    expect(hasMinimumCharacterCount('', 8)).toBe(true);
  });

  it('should return true when the character count is zero or less than zero', () => {
    expect(hasMinimumCharacterCount('', 0)).toBe(true);
    expect(hasMinimumCharacterCount('', -1)).toBe(true);
  });

  it('should return true when the character count is equal or more than the specified character count', () => {
    expect(hasMinimumCharacterCount('1234567', 7)).toBe(true);
    expect(hasMinimumCharacterCount('123456789', 7)).toBe(true);
  });

  it('should return false when the character count is less than the specified character count', () => {
    expect(hasMinimumCharacterCount('123456', 7)).toBe(false);
    expect(hasMinimumCharacterCount('123', 7)).toBe(false);
  });
});
