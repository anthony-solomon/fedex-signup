// Encapsulated this out to allow reuse across multiple validators
export function hasMinimumCharacterCount(value: string, minCharacterCount: number): boolean {
  if (minCharacterCount <= 0) {
    return true;
  }
  if (value.length === 0) {
    return true;
  }
  return value.length >= minCharacterCount;
}