// Encapsulated this out to allow reuse across multiple validators

  // In my experience, the stricter we are with an email validation, the sooner we get a support request asking
  // for us to allow a more permissive format because there are instances that we hadn't considered when building
  // the regular expression. I have scoured the web for a one size fits all usually go back to a simpler validation.
  
  // I find a double opt-in (or send an email to the user to confirm the address) would work best to ensure
  // that the email is valid.

  // There are, of course ready rolled regular expressions such as the one I used here because people have
  // taken the time verify them against the standards (in this case RFC 5322) and the comments reveal the 
  // weaknesses of the regex. This is compliments of http://emailregex.com/ which I felt was adequate for this
  // use case.
const emailRegEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export function isValidEmail(value: string): boolean {
  return emailRegEx.test(value);
}


