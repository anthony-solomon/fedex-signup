import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { AbstractControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-form-field-message',
  templateUrl: './form-field-message.component.html',
  styleUrls: ['./form-field-message.component.scss']
})
export class FormFieldMessageComponent implements OnChanges  {
  @Input() formGroup?: FormGroup; 
  @Input() controlName?: string | null = null;
  @Input() formFieldHint: string | null = null;
  @Input() errorMessageConfig: { [errorKey: string]: string } | null = null;

  control: AbstractControl | null | undefined = null;

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.formGroup && this.controlName) {
      this.control = this.formGroup?.get(this.controlName);
    }
  }
}
