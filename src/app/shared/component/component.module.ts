import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormFieldMessageComponent } from './form-field-message/form-field-message.component';

@NgModule({
  imports: [CommonModule],
  declarations: [FormFieldMessageComponent],
  exports: [FormFieldMessageComponent]
})
export class ComponentModule { }
