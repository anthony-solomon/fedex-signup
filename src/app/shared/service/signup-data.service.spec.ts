import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { inject, TestBed } from '@angular/core/testing';
import { API_URL, USER_ENDPOINT } from '@shared/constants';
import { UserPayload } from '@shared/interface/user-payload.interface';
import { SignupDataService } from './signup-data.service';

describe('SignupDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [HttpClient, SignupDataService]
    });
  });

  it('should post the user payload to the correct endpoint ', inject(
      [HttpTestingController, SignupDataService],
      (httpMock: HttpTestingController, service: SignupDataService) => {
    
    const mockPayload: UserPayload = {
      firstName: 'ThisIsTheTestUserFirstName',
      lastName: 'ThisIsTheTestUserLastName',
      email: 'testuser@email.com'
    }

    service.signup(mockPayload).subscribe();
    
    const targetUrl = `${API_URL}/${USER_ENDPOINT}`;
    const testRequest = httpMock.expectOne({ method: 'POST', url: targetUrl });

    testRequest.flush({});
    httpMock.verify();
  }));
});
