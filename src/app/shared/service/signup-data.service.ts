import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { API_URL, USER_ENDPOINT } from '@shared/constants';
import { Observable } from 'rxjs';
import { UserPayload } from '../interface/user-payload.interface';

@Injectable({
  providedIn: 'root'
})
export class SignupDataService {
  constructor(private readonly httpClient: HttpClient) {
  }

  signup(newUser: UserPayload): Observable<UserPayload> {
    return this.httpClient.post<UserPayload>(`${API_URL}/${USER_ENDPOINT}`, newUser);
  }
}
