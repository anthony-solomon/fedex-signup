export interface UserPayload {
  firstName: string;
  lastName: string;
  email: string;
}