// Service & endpoint configuration
export const API_URL = 'https://demo-api.now.sh'
export const USER_ENDPOINT = 'users';

// Password validation constants
export const PASSWORD_MIN_CHARACTER_COUNT = 8;

// The possible statuses from invoking the endpoint
export enum SignupStatus { NoStatus, Success, Error }

export const SignupStatusMessages = {
  [SignupStatus.Success]: 'The user was saved successfully.',
  [SignupStatus.Error]: 'There was an error while saving the user.',
}