import { Component } from '@angular/core';
import { SignupStatus, SignupStatusMessages } from '@shared/constants';
import { UserPayload } from '@shared/interface/user-payload.interface';
import { SignupDataService } from '@shared/service/signup-data.service';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  readonly SignupStatus = SignupStatus;
  readonly SignupStatusMessages = SignupStatusMessages;
  readonly title = 'FedEx Signup Form';
  
  signupStatus$ = new BehaviorSubject<SignupStatus>(SignupStatus.NoStatus);
  ngClass$ = this.signupStatus$.pipe(
    map((signupStatus: SignupStatus) => {
      if (signupStatus === SignupStatus.Success) return 'success';
      if (signupStatus === SignupStatus.Error) return 'error';
      return null; 
    })
  )

  constructor(private readonly signupDataService: SignupDataService) {
  }

  save(user: UserPayload): void {
    this.signupDataService.signup(user).subscribe(() => {
      this.signupStatus$.next(SignupStatus.Success);
    },
    () => {
      this.signupStatus$.next(SignupStatus.Error);
    });
  }
}
